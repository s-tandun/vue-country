# vue-country-lookup

Vue.js App for Country Lookup. A webpage that allows users to retrieve information about a country by using search input and entering its two-letter country code (alpha-2). The data is pulled from https://restcountries.com and https://countries.trevorblades.com/. For example:

- `US`: United States
- `GB`: Great Britain
- `CN`: China
- `TH`: Thailand

Live Site: https://s-tandun.gitlab.io/vue-country

## Functionalities

- Fetch country details
- Search for a country
- Store previous search

## Variable Reference

| Variable Name | API Data Type | Formatted Data Type | Description                                             |
| ------------- | ------------- | ------------------- | ------------------------------------------------------- |
| 'name'        | 'string'      | 'string'            | Name of the country                                     |
| 'capital'     | 'string'      | 'string'            | Name of the capital city                                |
| 'currency'    | 'string'      | 'string'            | Currency code used in the country (`USD`)               |
| 'call code'   | 'string'      | 'string'            | International dialing code for the country              |
| 'continent'   | 'object'      | 'string'            | Continent on which the country is located               |
| 'demonym'     | 'string'      | 'string'            | Noun used to denote people from the country             |
| 'area'        | 'int'         | 'string'            | Area in square kilometers (`km2`)                       |
| 'timezones'   | 'array'       | 'array'             | Array of local timezones                                |
| 'population'  | 'int'         | 'string'            | Total population in millions or billions, if applicable |
| 'languages'   | 'array'       | 'array'             | Array of primary spoken languages                       |

## Clone the project

```
git clone https://gitlab.com/s-tandun/vue-country.git
```

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
